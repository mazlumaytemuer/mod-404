﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace pingpong.v1
{
    public partial class PingPongForm : Form
    {
        private int _directionX = 5;
        private int _directionY = 2;

        public PingPongForm()
        {
            InitializeComponent();
        }

        private void StartGame(object sender, EventArgs e)
        {
            timer.Enabled = true;
            timer.Start();
        }

        private void EventHandler(object sender, EventArgs e)
        {
            // Bewegung des Balls
            ball.Location = new Point(ball.Location.X + _directionX, ball.Location.Y + _directionY);
            
            // Ball trifft auf rechten Spielfeldrand
            if (ball.Location.X >= gamePanel.Width - ball.Width)
            {
                _directionX = -_directionX;
            }

            // Ball trifft auf linken Spielfeldrand
            if (ball.Location.X <= 0)
            {
                _directionX = -_directionX;
            }

            // Ball trifft auf oberen Spielfeldrand
            if (ball.Location.Y >= gamePanel.Height - ball.Height)
            {
                _directionY = -_directionY;
            }

            // Ball trifft auf rechten Spielfeldrand
            if (ball.Location.Y < 0)
            {
                _directionY = -_directionY;
            }
        }
    }
}