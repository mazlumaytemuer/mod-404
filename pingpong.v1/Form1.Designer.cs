﻿namespace pingpong.v1
{
     partial class PingPongForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gamePanel = new System.Windows.Forms.Panel();
            this.start = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.ball = new System.Windows.Forms.PictureBox();
            this.gamePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.ball)).BeginInit();
            this.SuspendLayout();
            // 
            // gamePanel
            // 
            this.gamePanel.BackColor = System.Drawing.Color.SeaGreen;
            this.gamePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gamePanel.Controls.Add(this.ball);
            this.gamePanel.Location = new System.Drawing.Point(27, 39);
            this.gamePanel.Name = "gamePanel";
            this.gamePanel.Size = new System.Drawing.Size(527, 289);
            this.gamePanel.TabIndex = 0;
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(30, 366);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(103, 51);
            this.start.TabIndex = 1;
            this.start.Text = "Spiel starten";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.StartGame);
            // 
            // timer
            // 
            this.timer.Interval = 120;
            this.timer.Tick += new System.EventHandler(this.EventHandler);
            // 
            // ball
            // 
            this.ball.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.ball.Location = new System.Drawing.Point(70, 90);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(25, 25);
            this.ball.TabIndex = 0;
            this.ball.TabStop = false;
            // 
            // PingPongForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 501);
            this.Controls.Add(this.start);
            this.Controls.Add(this.gamePanel);
            this.Name = "PingPongForm";
            this.Text = "Ping-Pong Spiel";
            this.gamePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.ball)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel gamePanel;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.PictureBox ball;
    }
}