﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PinpPong
{
    public partial class PingPongForm : Form
    {
        private bool _directionX;
        private bool _directionY;
        private int _point;
        public PingPongForm()
        {
            InitializeComponent();
        }

        private void StartGame(object sender, EventArgs e)
        {
            timer.Enabled = true;
            timer.Start();
        }

        private void EventHandler(object sender, EventArgs e)
        { 
            // Ball trifft auf rechten Spielfeldrand
            if (ball.Location.X + ball.Width >= pnlSpiel.Width)
            {
                _directionX = false;
            }

            // Ball trifft auf linken Spielfeldrand
            if (ball.Location.X <= 0)
            {
                _directionX = true;
            }

            // Ball trifft auf unteren Spielfeldrand
            if (ball.Location.Y >= pnlSpiel.Height - ball.Height)
            {
                _directionY = false;
            }

            // Ball trifft auf oberen Spielfeldrand
            if (ball.Location.Y <= 0)
            {
                _directionY = true; 
            }

            //Bewegung
            if(_directionX)
                ball.Location = new Point(ball.Location.X + 1, ball.Location.Y);
            else ball.Location = new Point(ball.Location.X - 1, ball.Location.Y);
            if (_directionY)
                ball.Location = new Point(ball.Location.X, ball.Location.Y + 1);
            else ball.Location = new Point(ball.Location.X, ball.Location.Y - 1);

            //hitbox
            if (ball.Location.X + ball.Width >= picSchlaegerRechts.Location.X)
            {
               for (int i = 1; i < ball.Height; i++)
                {
                    if(i + ball.Location.Y >= picSchlaegerRechts.Location.Y && i + ball.Location.Y <= picSchlaegerRechts.Location.Y + picSchlaegerRechts.Height && _directionX)
                    {
                        _directionX = false;
                        _point = _point + 10;
                        txtPunkte.Text = Convert.ToString(_point);
                    }
                }
            }
        }

        private void PingPongForm_Load(object sender, EventArgs e)
        {
            //Definiert die Position des Schlägers auf dem GamePanel. picSchlaegerRechts.Width*2 mach das der Schläger sich um *2 nach vorne bewegt.
            picSchlaegerRechts.Location = new Point(pnlSpiel.Width - picSchlaegerRechts.Width, pnlSpiel.Height / 2);
            //Legt die Position der Bildlaufleiste auf dem Formular fest.
            vsbSchlaegerRechts.Location = new Point(pnlSpiel.Location.X + pnlSpiel.Width, pnlSpiel.Location.Y);
            //Legt den Mindestwert der Bildlaufleiste fest.
            vsbSchlaegerRechts.Minimum = 0;
            //Maximalen Wert der Bildlaufleiste.
            vsbSchlaegerRechts.Maximum = pnlSpiel.Height - picSchlaegerRechts.Height + vsbSchlaegerRechts.LargeChange;
            //Legt dass die Höhe der Bildlaufleiste mirt der des GamePanels.
            vsbSchlaegerRechts.Height = pnlSpiel.Height;
            //Prüft ob Bildlaufleiste gleich der vertikalen Position des Schlägers ist.
            vsbSchlaegerRechts.Value = picSchlaegerRechts.Location.Y;
        }
           
        private void vsbSchlaegerRechts_Scroll(object sender, ScrollEventArgs e)
        {
            //Neuer Possition erstellt, X stellt Possition von picSchlaegerRechts-Bildes fest / Y stell Possition von = vsbSchlaegerRechts-Schlägers, fest.
            picSchlaegerRechts.Location = new Point(picSchlaegerRechts.Location.X, vsbSchlaegerRechts.Value);
        }

    
    }

}
