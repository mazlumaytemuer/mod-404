using System;
using System.Windows.Forms;

namespace pingpong.v1
{
    internal static class PingPong
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PingPongForm());
        }
    }
}