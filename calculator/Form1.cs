﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace calculator
{
    public partial class CalculatorForm : Form
    {
        private double _number1;
        private double _number2;
        private double _result;

        public CalculatorForm()
        {
            InitializeComponent();
        }

        private void btnAddition_Click(object sender, EventArgs e)
        {
            _number1 = Convert.ToDouble(firstOperand.Text);
            _number2 = Convert.ToDouble(secondOperand.Text);
            _result = _number1 + _number2;

            lblErgebnis.Text = Convert.ToString(_result, CultureInfo.CurrentCulture);
            lblOperator.Text = @"+";
        }

        private void btnSubtraktion_Click(object sender, EventArgs e)
        {
            _number1 = Convert.ToDouble(firstOperand.Text);
            _number2 = Convert.ToDouble(secondOperand.Text);
            _result = _number1 - _number2;

            lblErgebnis.Text = Convert.ToString(_result, CultureInfo.CurrentCulture);
            lblOperator.Text = @"-";
        }

        private void btnMittelwert_Click(object sender, EventArgs e)
        {
            _number1 = Convert.ToDouble(firstOperand.Text);
            _number2 = Convert.ToDouble(secondOperand.Text);
            _result = (_number1 + _number2) / 2;
            lblErgebnis.Text = Convert.ToString(_result, CultureInfo.CurrentCulture);
            lblOperator.Text = @"Ø";
        }

        private void btnPotenz_Click(object sender, EventArgs e)
        {
            _number1 = Convert.ToDouble(firstOperand.Text);
            _number2 = Convert.ToDouble(secondOperand.Text);
            _result = Math.Pow(_number1, _number2);

            lblErgebnis.Text = Convert.ToString(_result, CultureInfo.CurrentCulture);
            lblOperator.Text = @"^";
        }

        private void btnMaximum_Click(object sender, EventArgs e)
        {
            _number1 = Convert.ToDouble(firstOperand.Text);
            _number2 = Convert.ToDouble(secondOperand.Text);
            _result = _number1;

            if (_number1 < _number2) _result = _number2;

            lblErgebnis.Text = Convert.ToString(_result, CultureInfo.CurrentCulture);
            lblOperator.Text = @"Max";
        }
    }
}